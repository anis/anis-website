UID := $(id -u)
GID := $(id -g)
NAME_APP=anis-website

list:
	@echo ""
	@echo "Useful targets:"
	@echo ""
	@echo "  start        > run a dev server for $(NAME_APP) application (in memory)"
	@echo "  stop         > stop the dev server for $(NAME_APP) application"
	@echo "  restart      > restart the dev server for $(NAME_APP) (container)"
	@echo "  status       > display $(NAME_APP) container status"

start:
	@docker run --rm --name $(NAME_APP) -d -p 8888:80 -v $(CURDIR)/src:/usr/share/nginx/html:ro nginx

restart: stop start

stop:
	@docker stop $(NAME_APP)

restart: stop start

status:
	@docker ps -f name=$(NAME_APP)
